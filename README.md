Master Branch

**installation**

*  Download Go for your OS [here](https://golang.org/dl/)
*  run `go get gitlab.com/causevest/xcv-protocol-go`
*  `cd gitlab.com/causevest/xcv-protocol-go/server`
*  `go get -u` 
*  `make build` 

You can also make the cli tool using 

*  `cd gitlab.com/causevest/xcv-protocol-go/cli`
*  `go get -u`
*  `make build`
